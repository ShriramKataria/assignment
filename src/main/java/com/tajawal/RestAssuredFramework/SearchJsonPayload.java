package com.tajawal.RestAssuredFramework;

import com.tajawal.helper.DateHelper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import java.util.LinkedHashMap;


public class SearchJsonPayload {
    static int jsonArrayIndex=0;

    public static LinkedHashMap setDatesInfo(int bookingAfter,int noOfDaysStay) throws JSONException {
        LinkedHashMap<String, String> datesInfo=new LinkedHashMap<>();
        DateHelper dateHelper=new DateHelper();
        String checkInDate=dateHelper.getDate(bookingAfter);
        String checkOutDate=dateHelper.getDate(bookingAfter+noOfDaysStay);

        datesInfo.put("checkin",checkInDate);
        datesInfo.put("checkout",checkOutDate);
        return datesInfo;
    }

    public static JSONArray setRoomsAndGuests(String noOfRooms,String guests) throws JSONException {

        LinkedHashMap<String, String> guestMap = new LinkedHashMap<>();
        JSONArray roomArray=new JSONArray();
        String[] guestsInfoPerRoom=guests.split("~");

        for (int roomInit=0;roomInit<Integer.parseInt(noOfRooms);roomInit++) {
            int adtInit=0;
            int chdInit=0;
            JSONArray guestArray=new JSONArray();
            JSONObject guestObj=new JSONObject();
            int noOfAdult=Integer.parseInt((guestsInfoPerRoom[roomInit].split("-")[0].substring(0,1)));
            int noOfChild=Integer.parseInt((guestsInfoPerRoom[roomInit].split("-")[1].substring(0, 1)));
            String childString = guestsInfoPerRoom[roomInit].split("-")[1];

            while (adtInit <noOfAdult) {
                guestMap.put("type", "ADT");
                guestArray.put(jsonArrayIndex, guestMap);
                jsonArrayIndex++;
                adtInit++;
            }
            while (chdInit < noOfChild) {

                int index1=childString.indexOf("(");
                int index2=childString.indexOf(")");
                String[] childAge=childString.substring(index1+1,index2).split("&");
                guestMap.put("type", "CHD");
                guestMap.put("age", childAge[chdInit]);
                guestArray.put(jsonArrayIndex, guestMap);
                jsonArrayIndex++;
                chdInit++;
            }

            guestObj.put("guest",guestArray);
            roomArray.put(roomInit,guestObj);
            jsonArrayIndex=0;
            guestMap.clear();

        }
        return roomArray;
    }

    public JSONObject searchRequestBody(String bookingAfter, String noOfDaysStay, String destination, String noOfRooms, String guests, String placeId) throws JSONException {
        JSONObject jsonBodObj=new JSONObject();
        jsonBodObj.put("dates",setDatesInfo(Integer.parseInt(bookingAfter),Integer.parseInt(noOfDaysStay)));
        jsonBodObj.put("destination",destination);
        jsonBodObj.put("room",setRoomsAndGuests(noOfRooms,guests));
        jsonBodObj.put("placeId",placeId);
        return jsonBodObj;


    }



}


