package com.tajawal.RestAssuredFramework;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.internal.UriValidator;
import com.jayway.restassured.response.Response;
import com.tajawal.helper.PropertyFileReader;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class GeoSuggestImplementation {
    public static Response response;
    public static Properties configFile = PropertyFileReader.getConfigPropertyFile();



    public Response sendGeoSuggestRequest(Map<String,String> cookies,String value) {

        RestAssured.baseURI = configFile.getProperty("GeoSuggestBaseURI");
        response = RestAssured.given().
                param("query", value).
                cookies(cookies).
                header("Content-Type", "application/json").
                when().
                get(configFile.getProperty("GeoSuggestURL"));

//        System.out.println("********************************Response*****************************");
//
//        System.out.println(response.statusCode());
//        System.out.println(response.asString());
//
//        System.out.println("********************************Cookies*****************************");
//        System.out.println(response.getCookies());
//
//        System.out.println("********************************Headers*****************************");
//        System.out.println(response.getHeaders());
        return response;
    }

    public void performSchemaVal() {

    response.then().assertThat().body(matchesJsonSchemaInClasspath("JsonSchema/GeoSuggest.json"));

    }

    public void isBlank(String fieldName) {
        Assert.assertTrue(fieldName+"is Blank",!fieldName.isEmpty());
    }

    public void validateHotelId(HashMap<String,String>hotelInfo) {


        int hotelId = Integer.parseInt(String.valueOf(hotelInfo.get("hotelId")));
        System.out.println("Hotel Id="+hotelId);
        isBlank(Integer.toString(hotelId));
        Assert.assertTrue("Hotel Id is not valid",hotelId>0);
    }

    public void validateHotelName(HashMap<String,String>hotelInfo) {
        String hotelName=hotelInfo.get("name");
        System.out.println("Hotel Name="+hotelName);
        isBlank(hotelName);
    }

    public void validateHotelCity(HashMap<String,String>hotelInfo) {
        String hotelCity=hotelInfo.get("city");
        System.out.println("Hotel City="+hotelCity);
        isBlank(hotelCity);
    }


    public void validateHotelCountry(HashMap<String,String>hotelInfo) {
        String hotelCountry=hotelInfo.get("country");
        System.out.println("Country name="+hotelCountry);
        isBlank(hotelCountry);
    }

    public void validateDisplayType(HashMap<String,String>hotelInfo) {
        String displayType=hotelInfo.get("displayType");
        System.out.println("Display Type="+displayType);
        isBlank(displayType);
        Assert.assertTrue("Display Type is not correct", displayType.equals("Hotel"));
    }

    public void validateHotelImageURL(HashMap<String,String>hotelInfo) {
        String hotelImageURL=hotelInfo.get("thumbnail_url");
        System.out.println("Hotel image URL="+hotelImageURL);
        isBlank(hotelImageURL);
        Assert.assertTrue("Hotel image URL" + hotelImageURL+"is not valid",UriValidator.isUri(hotelImageURL));

    }

    public void validateLatitude(HashMap<String,String>hotelInfo) {
        Float latitude= Float.parseFloat(String.valueOf(hotelInfo.get("latitude")));
        System.out.println("Hotel latitude="+latitude);
        isBlank(latitude.toString());
    }

    public void validateLongitude(HashMap<String,String>hotelInfo) {
        Float longitude= Float.parseFloat(String.valueOf(hotelInfo.get("longitude")));
        System.out.println("Hotel longitude="+longitude);
        isBlank(longitude.toString());
    }

    public void validateLocationName(HashMap<String,String>locationInfo) {
        String locationName=locationInfo.get("name");
        isBlank(locationName);
    }

    public void validatePlaceId(HashMap<String,String>locationInfo) {
        String placeId=locationInfo.get("placeId");
        isBlank(placeId);
    }

    public void validateSource(HashMap<String,String>locationInfo) {
        String source=locationInfo.get("source");
        isBlank(source);
    }

    public void validateLocationCity(HashMap<String,String>locationInfo) {
        String city=locationInfo.get("city");
        isBlank(city);
    }

    public void validateLocationCountry(HashMap<String,String>locationInfo) {
        String country=locationInfo.get("country");
        isBlank(country);
    }

    public void validateLocationDisplayType(HashMap<String,String>locationInfo) {
        String displayType=locationInfo.get("displayType");
        isBlank(displayType);
    }



    public void validateHotelDetails() {

        ArrayList<HashMap<String, String>> hotelsInfo = (ArrayList<HashMap<String, String>>) response.jsonPath()
                                                         .get("hotels");
        for(HashMap<String, String> hotelInfo:hotelsInfo ) {
            validateHotelId(hotelInfo);
            validateHotelName(hotelInfo);
            validateHotelCity(hotelInfo);
            validateHotelCountry(hotelInfo);
            validateHotelImageURL(hotelInfo);
            validateDisplayType(hotelInfo);
            validateLatitude(hotelInfo);
            validateLongitude(hotelInfo);
        }
    }


    public void validateLocationDetails() {

        ArrayList<HashMap<String, String>> locationsInfo = (ArrayList<HashMap<String, String>>) response.jsonPath()
                .get("locations");
        for(HashMap<String, String> locationInfo:locationsInfo ) {
            validateLocationName(locationInfo);
            validatePlaceId(locationInfo);
            validateSource(locationInfo);
            validateLocationCity(locationInfo);
            validateLocationCountry(locationInfo);
            validateLocationDisplayType(locationInfo);

        }
    }


    public void getResponseHeader() {
        response.getHeaders();
        System.out.println(response.getHeaders());
    }



} 