package com.tajawal.RestAssuredFramework;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.tajawal.helper.DateHelper;
import com.tajawal.helper.PropertyFileReader;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class SearchServiceImplementation {
    public static Response response=null;
    public static Properties configFile = PropertyFileReader.getConfigPropertyFile();


    public Response sendSearchRequest(Map<String, String> cookies, String jsonPayload) {

        response = RestAssured.given().
                cookies(cookies).log().all().
                header("Content-Type", "application/json").
                body(jsonPayload).
                when().
                post(configFile.getProperty("SearchEndPointURL"));

        System.out.println("********************************Response*****************************");

        System.out.println(response.statusCode());
        System.out.println(response.asString());

        System.out.println("********************************Cookies*****************************");
        System.out.println(response.getCookies());

        System.out.println("********************************Headers*****************************");
        System.out.println(response.getHeaders());
        return response;
    }

    public void performSchemaVal() {

        response.then().assertThat().body(matchesJsonSchemaInClasspath("JsonSchema/SearchRequest.json"));

    }



    public static void isBlank(String fieldName) {
        Assert.assertTrue(fieldName + "is Blank", !fieldName.isEmpty());
    }

    public static void validateType(String query) {
        String typeValue=response.jsonPath().getString("type");
        isBlank(typeValue);
        Assert.assertTrue("type field value is not correct",query.equalsIgnoreCase(query));
    }

    public static void validateQuery(String destination, String guests,String bookingAfter,String noOfDaysStay) {
        DateHelper dateHelper=new DateHelper();

          String checkInDate=dateHelper.getDate(Integer.parseInt(bookingAfter));
          String checkOutDate=dateHelper.getDate(Integer.parseInt(bookingAfter)+Integer.parseInt(noOfDaysStay));
          String queryValue=response.jsonPath().getString("query");
          isBlank(queryValue);
          String query=destination.toLowerCase()+"/"+checkInDate+"/"+checkOutDate+"/";
          Assert.assertTrue("Query field value is not correct",queryValue.contains(query.trim()));
    }


    public static void validateQueryParameters(String placeId) {
        String queryParametersValue=response.jsonPath().getString("queryParameters");
        isBlank(queryParametersValue);
        Assert.assertTrue("Query Parameters does not have placeId", (queryParametersValue.contains(placeId)));

    }

    public static void validatePlaceId(String placeId) {
        String placeIdValue=response.jsonPath().getString("queryParametersObj.placeId");
        isBlank(placeIdValue);
        Assert.assertTrue("Place id is not correct", (placeIdValue.contains(placeId)));

    }


    public void validateSearchServiceResponse(String bookingAfter, String noOfDaysStay, String destination, String guests, String placeId) {
        validateQueryParameters(placeId);
        validateType("hotel");
        validateQuery(destination, guests,bookingAfter,noOfDaysStay);
        validatePlaceId(placeId);

    }
}
