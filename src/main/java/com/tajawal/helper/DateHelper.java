package com.tajawal.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateHelper {
    public static String getDate(int days) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, days);
        return simpleDateFormat.format(calender.getTime());
    }
} 