package com.tajawal.helper;

import com.jayway.restassured.response.Response;

public class RestAssuredUtil {
    public int getNumberOfCookies(Response response) {
        int noOfCookies = response.getCookies().size();
        return noOfCookies;
    }
} 