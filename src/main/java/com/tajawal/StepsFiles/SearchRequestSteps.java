package com.tajawal.StepsFiles;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.tajawal.RestAssuredFramework.SearchJsonPayload;
import com.tajawal.RestAssuredFramework.SearchServiceImplementation;
import com.tajawal.helper.PropertyFileReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.junit.Assert;
import org.junit.validator.PublicClassValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SearchRequestSteps {
    public static Response response=null;
    Map<String,String> cookies=new HashMap<String, String>();
    public static SearchJsonPayload SearchJsonPayload=new SearchJsonPayload();
    public static Properties configFile = PropertyFileReader.getConfigPropertyFile();
    public static SearchServiceImplementation searchServiceImplementation=new SearchServiceImplementation();


    @When("^Send search service request for details \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void send_search_service_request(String bookingAfter, String noOfDaysStay, String destination, String noOfRooms, String guests, String placeId) throws Throwable {
        String jsonPayload=SearchJsonPayload.searchRequestBody(bookingAfter,noOfDaysStay,destination,noOfRooms,guests,placeId).toString();
        response=searchServiceImplementation.sendSearchRequest(cookies,jsonPayload);

    }

    @Then("^Verify Error message \"([^\"]*)\" should be displayed$")
    public void Error_message_should_be_displayed(String expectedErrorMessage) throws Throwable {
        int statusCode=0;
        String actualErrorMessage=null;
        try {
             statusCode = response.jsonPath().get("status");
             actualErrorMessage=response.jsonPath().get("detail").toString();
        }
        catch (Exception e) {
            Assert.assertTrue("Getting response for past date as well",false);
        }


        Assert.assertEquals(statusCode,400);
        Assert.assertTrue("Error Message is not correct",actualErrorMessage.contains(expectedErrorMessage));

    }

    @Then("^Verify service response for search service \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void verify_service_response_for_hotel(String bookingAfter, String noOfDaysStay, String destination, String guests, String placeId) throws Throwable {
        searchServiceImplementation.validateSearchServiceResponse(bookingAfter, noOfDaysStay, destination, guests,  placeId);
    }



    @Then("^User should get service response$")
    public void user_should_get_service_response() throws Throwable {
       int actualStatusCode=response.getStatusCode();
       Assert.assertEquals(actualStatusCode, 200);

    }

    @Then("^Perform schema validation for search service response$")
    public void perform_schema_validation_for_search_service_response() throws Throwable {
        searchServiceImplementation.performSchemaVal();

    }

    @Then("^Verify that number of cookies in search response should be \"([^\"]*)\" when response came first time$")
    public void Verify_that_number_of_cookies(String expectedNumberOfCookies) throws Throwable {
        int noOfCookies = response.getCookies().size();
        Assert.assertEquals(noOfCookies,Integer.parseInt(expectedNumberOfCookies));
    }

    @Then("^Verify cookies Keys text received from server for search request$")
    public void Verify_cookies_Keys_text_received_from_server_for_search_request() throws Throwable {
        cookies=response.getCookies();
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie1Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie2Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie3Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie4Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie5Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie6Key")) );

    }


    @Then("^Verify Max age of cookies for search response should be \"([^\"]*)\"$")
    public void Verify_expiry_of_cookies(String maxAge) throws Throwable {
        for(int i=0;i<3;i++) {
            int cookieMaxAge = response.getDetailedCookie(configFile.getProperty("Cookie"+(i+4)+"Key")).getMaxAge();
            Assert.assertEquals(cookieMaxAge, Integer.parseInt(maxAge));
        }

    }









} 