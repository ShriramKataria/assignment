package com.tajawal.StepsFiles;

import com.tajawal.RestAssuredFramework.GeoSuggestImplementation;
import com.jayway.restassured.response.Response;
import com.tajawal.helper.PropertyFileReader;
import com.tajawal.helper.RestAssuredUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Properties;

public class GeoSuggestSteps {

    public static Response response;
    Map<String,String> cookies=new HashMap<String, String>();
    public static GeoSuggestImplementation geoSuggestService=new GeoSuggestImplementation();
    public static Properties configFile = PropertyFileReader.getConfigPropertyFile();


    @When("^Send geo suggest service request for \"([^\"]*)\"$")
    public void send_geo_suggest_service_request_for(String value) throws Throwable {

        response=geoSuggestService.sendGeoSuggestRequest(cookies,value);
//        response.prettyPrint();
//        cookies= response.getCookies();
//        System.out.println(cookies);

    }

    @Then("^Verify cookies Keys text received from server$")
    public void Verify_cookies_Keys_text_received_from_server() throws Throwable {
        cookies=response.getCookies();
        System.out.println(cookies);
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie1Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie2Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie3Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie4Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie5Key")) );
        Assert.assertTrue("Cookies Key is invalid",cookies.containsKey(configFile.getProperty("Cookie6Key")) );

    }

    @Then("^Verify that number of cookies should be \"([^\"]*)\" when response came first time$")
    public void Verify_that_number_of_cookies(String expectedNumberOfCookies) throws Throwable {
        int noOfCookies = response.getCookies().size();
        Assert.assertEquals(noOfCookies,Integer.parseInt(expectedNumberOfCookies));
    }

    @Then("^Verify that Max age of cookies should be \"([^\"]*)\"$")
    public void Verify_expiry_of_cookies(String maxAge) throws Throwable {
        for(int i=0;i<3;i++) {
            int cookieMaxAge = response.getDetailedCookie(configFile.getProperty("Cookie"+(i+4)+"Key")).getMaxAge();
            Assert.assertEquals(cookieMaxAge, Integer.parseInt(maxAge));
        }

    }



    @Then("^Perform schema validation for geosuggest service response$")
    public void perform_schema_validation_for_geo_suggest_service() throws Throwable {
        geoSuggestService.performSchemaVal();

    }



    @Then("^Verify service response for hotel$")
    public void verify_service_response_for_hotel() throws Throwable {
        geoSuggestService.validateHotelDetails();
    }

    @Then("^Verify header response$")
    public void verify_header_response() throws Throwable {
        geoSuggestService.getResponseHeader();
    }




    @And("^Verify service response for Location$")
    public void verify_service_response_for_Location() throws Throwable {
        geoSuggestService.validateLocationDetails();
    }
}
