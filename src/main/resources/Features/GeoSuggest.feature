Feature: This feature file is to verify geo-suggest service


    @GeoSuggestSchemaValidation
    Scenario Outline: Verify geo suggest service response
      When Send geo suggest service request for "<Query>"
      Then Perform schema validation for geosuggest service response

      Examples:
        |Query                       |
        |Paris                       |
        |India                       |
        |Mahua Kothi Bandhavgarh     |
        |Premier Inn                 |


    @GeoSuggestResponseValidation_ForHotelObject
    Scenario Outline: Verify geo suggest service response for hotel object
      When Send geo suggest service request for "<Query>"
      Then Verify service response for hotel


      Examples:
       |Query    |
       |Paris    |
       |India    |



  @GeoSuggestResponseValidation_ForLocationObject
  Scenario Outline: Verify geo suggest service response for Location object
    When Send geo suggest service request for "<Query>"
    Then  Verify service response for Location
    Examples:

      |Query         |
      |Premier Inn   |
      |Paris         |
      |India         |


      @GeoSuggestCookiesKeysValidation
    Scenario: Verify cookies size and cookies Keys
       When Send geo suggest   service request for "India"
       Then Verify that number of cookies should be "6" when response came first time
       And  Verify cookies Keys text received from server
       And Verify that Max age of cookies should be "900"








