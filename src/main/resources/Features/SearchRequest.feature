Feature: This feature file is to test "Search" request


  @SearchSchemaValidation
  Scenario Outline: Verify geo suggest service response
    When Send search service request for details "<Booking After>", "<No of Days Stays>", "<Destination>", "<No of Rooms>", "<Guests>", "<placeId>"
    Then Perform schema validation for search service response

    Examples:
      |Booking After|No of Days Stays|Destination           |No of Rooms|Guests                                |placeId                      |
      |10           |3               |India                 |2          |1ADT-2CHD(3&12)~2ADT-0CHD             |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |


  @SearchServiceCookiesKeysValidation
  Scenario Outline: Verify cookies size, cookies Keys and Max Age
    When Send search service request for details "<Booking After>", "<No of Days Stays>", "<Destination>", "<No of Rooms>", "<Guests>", "<placeId>"
    Then Verify that number of cookies in search response should be "6" when response came first time
    And  Verify cookies Keys text received from server for search request
    And  Verify Max age of cookies for search response should be "900"

    Examples:
      |Booking After|No of Days Stays|Destination           |No of Rooms|Guests                                |placeId                      |
      |10           |3               |India                 |2          |1ADT-2CHD(3&12)~2ADT-0CHD             |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |



  @SearchHotelBooking
  Scenario Outline: This test case is to verify search request with multiple test data
    #Guest Details :1ADT-2CHD(3&12)~2ADT-0CHD {After 2CHD (3&12), It's age of child}
    #Guest Details :1ADT-2CHD(3&12)~2ADT-0CHD {after split "~", detail of guests of each room}
    When Send search service request for details "<Booking After>", "<No of Days Stays>", "<Destination>", "<No of Rooms>", "<Guests>", "<placeId>"
    Then User should get service response
    And  Verify service response for search service "<Booking After>", "<No of Days Stays>", " <Destination>", "<Guests>", "<placeId>"

    Examples:
    |Booking After|No of Days Stays|Destination           |No of Rooms|Guests                                |placeId                      |
    |10           |3               |India                 |2          |1ADT-2CHD(3&12)~2ADT-0CHD              |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |
    |40           |15              |UnitedArabEmirates    |1          |2ADT-1CHD(3)                           |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |
    |365          |1               |Maldives              |1          |2ADT-1CHD(3)                           |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |
    |20           |1               |Maldives              |1          |2ADT-1CHD(3)~0ADT-1CHD(5)              |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |
    |50           |1               |Nepal                 |2          |2ADT-1CHD(3)~0ADT-1CHD(22)             |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |
    |50           |1               |Nepal                 |3          |2ADT-1CHD(7)~1ADT-2CHD(22&10)~3ADT-0CHD|ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |


  @PastCheckInDate
  Scenario Outline: Error message should be displayed if check-in date is past date

    When Send search service request for details "<Booking After>", "<No of Days Stays>", "<Destination>", "<No of Rooms>", "<Guests>", "<placeId>"
    Then Verify Error message "<Error Message>" should be displayed
    Examples:
      |Booking After|No of Days Stays|Destination           |No of Rooms|Guests                                |placeId                      |Error Message                          |
      |-1           |3               |India                 |2          |1ADT-2CHD(3&12)~2ADT-0CHD             |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |The dates.checkin must be a date after |


  @CheckOutDateLessThanOrSame_WithCheckInDate
  Scenario Outline: Error message should be displayed if checkout date is same or less than check-in date

    When Send search service request for details "<Booking After>", "<No of Days Stays>", "<Destination>", "<No of Rooms>", "<Guests>", "<placeId>"
    Then Verify Error message "<Error Message>" should be displayed
    Examples:
      |Booking After|No of Days Stays|Destination           |No of Rooms|Guests                                |placeId                      |Error Message                                         |
      |5            |-2              |India                 |2          |1ADT-2CHD(3&12)~2ADT-0CHD             |ChIJD7fiBh9u5kcRYJSMaMOCCwQ  |The dates.checkout must be a date after dates.checkin |

