package RunnerClasses;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/html-reports/SearchRequest",
                "json:target/cucumber-report/SearchRequest"},
        features = {"src/main/resources/Features/SearchRequest.feature"},
        glue = {"com.tajawal.StepsFiles"}
        )
public class SearchRequestTest {
} 