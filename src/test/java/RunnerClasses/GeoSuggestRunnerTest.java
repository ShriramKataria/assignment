package RunnerClasses;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/html-reports/GeoSuggestSchemaValidation",
                "json:target/cucumber-report/GeoSuggestSchemaValidation"},
        features = {"src/main/resources/Features/GeoSuggest.feature"},
        glue = {"com.tajawal.StepsFiles"})

public class GeoSuggestRunnerTest {
} 