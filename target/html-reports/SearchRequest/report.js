$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/resources/Features/SearchRequest.feature");
formatter.feature({
  "line": 1,
  "name": "This feature file is to test \"Search\" request",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Verify geo suggest service response",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-geo-suggest-service-response",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@SearchSchemaValidation"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Send search service request for details \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \"\u003cDestination\u003e\", \"\u003cNo of Rooms\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Perform schema validation for search service response",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-geo-suggest-service-response;",
  "rows": [
    {
      "cells": [
        "Booking After",
        "No of Days Stays",
        "Destination",
        "No of Rooms",
        "Guests",
        "placeId"
      ],
      "line": 10,
      "id": "this-feature-file-is-to-test-\"search\"-request;verify-geo-suggest-service-response;;1"
    },
    {
      "cells": [
        "10",
        "3",
        "India",
        "2",
        "1ADT-2CHD(3\u002612)~2ADT-0CHD",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"
      ],
      "line": 11,
      "id": "this-feature-file-is-to-test-\"search\"-request;verify-geo-suggest-service-response;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Verify geo suggest service response",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-geo-suggest-service-response;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@SearchSchemaValidation"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Send search service request for details \"10\", \"3\", \"India\", \"2\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Perform schema validation for search service response",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 41
    },
    {
      "val": "3",
      "offset": 47
    },
    {
      "val": "India",
      "offset": 52
    },
    {
      "val": "2",
      "offset": 61
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 66
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1165035951,
  "status": "passed"
});
formatter.match({
  "location": "SearchRequestSteps.perform_schema_validation_for_search_service_response()"
});
formatter.result({
  "duration": 8429205,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 15,
  "name": "Verify cookies size, cookies Keys and Max Age",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-cookies-size,-cookies-keys-and-max-age",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 14,
      "name": "@SearchServiceCookiesKeysValidation"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "Send search service request for details \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \"\u003cDestination\u003e\", \"\u003cNo of Rooms\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "Verify that number of cookies in search response should be \"6\" when response came first time",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Verify cookies Keys text received from server for search request",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Verify Max age of cookies for search response should be \"900\"",
  "keyword": "And "
});
formatter.examples({
  "line": 21,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-cookies-size,-cookies-keys-and-max-age;",
  "rows": [
    {
      "cells": [
        "Booking After",
        "No of Days Stays",
        "Destination",
        "No of Rooms",
        "Guests",
        "placeId"
      ],
      "line": 22,
      "id": "this-feature-file-is-to-test-\"search\"-request;verify-cookies-size,-cookies-keys-and-max-age;;1"
    },
    {
      "cells": [
        "10",
        "3",
        "India",
        "2",
        "1ADT-2CHD(3\u002612)~2ADT-0CHD",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"
      ],
      "line": 23,
      "id": "this-feature-file-is-to-test-\"search\"-request;verify-cookies-size,-cookies-keys-and-max-age;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 23,
  "name": "Verify cookies size, cookies Keys and Max Age",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;verify-cookies-size,-cookies-keys-and-max-age;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 14,
      "name": "@SearchServiceCookiesKeysValidation"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "Send search service request for details \"10\", \"3\", \"India\", \"2\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "Verify that number of cookies in search response should be \"6\" when response came first time",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Verify cookies Keys text received from server for search request",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Verify Max age of cookies for search response should be \"900\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 41
    },
    {
      "val": "3",
      "offset": 47
    },
    {
      "val": "India",
      "offset": 52
    },
    {
      "val": "2",
      "offset": 61
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 66
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1449184415,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 60
    }
  ],
  "location": "SearchRequestSteps.Verify_that_number_of_cookies(String)"
});
formatter.result({
  "duration": 312319,
  "status": "passed"
});
formatter.match({
  "location": "SearchRequestSteps.Verify_cookies_Keys_text_received_from_server_for_search_request()"
});
formatter.result({
  "duration": 289564,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "900",
      "offset": 57
    }
  ],
  "location": "SearchRequestSteps.Verify_expiry_of_cookies(String)"
});
formatter.result({
  "duration": 288426,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 28,
  "name": "This test case is to verify search request with multiple test data",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 27,
      "name": "@SearchHotels"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {After 2CHD (3\u002612), It\u0027s age of child}"
    },
    {
      "line": 30,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {after split \"~\", detail of guests of each room}"
    }
  ],
  "line": 31,
  "name": "Send search service request for details \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \"\u003cDestination\u003e\", \"\u003cNo of Rooms\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User should get service response",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Verify service response for search service \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \" \u003cDestination\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 35,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;",
  "rows": [
    {
      "cells": [
        "Booking After",
        "No of Days Stays",
        "Destination",
        "No of Rooms",
        "Guests",
        "placeId"
      ],
      "line": 36,
      "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;1"
    },
    {
      "cells": [
        "10",
        "3",
        "India",
        "2",
        "1ADT-2CHD(3\u002612)~2ADT-0CHD",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"
      ],
      "line": 37,
      "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;2"
    },
    {
      "cells": [
        "40",
        "15",
        "UnitedArabEmirates",
        "1",
        "2ADT-1CHD(3)",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"
      ],
      "line": 38,
      "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;3"
    },
    {
      "cells": [
        "365",
        "1",
        "Maldives",
        "1",
        "2ADT-1CHD(3)",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ"
      ],
      "line": 39,
      "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 37,
  "name": "This test case is to verify search request with multiple test data",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 27,
      "name": "@SearchHotels"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {After 2CHD (3\u002612), It\u0027s age of child}"
    },
    {
      "line": 30,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {after split \"~\", detail of guests of each room}"
    }
  ],
  "line": 31,
  "name": "Send search service request for details \"10\", \"3\", \"India\", \"2\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User should get service response",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Verify service response for search service \"10\", \"3\", \" India\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    4,
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 41
    },
    {
      "val": "3",
      "offset": 47
    },
    {
      "val": "India",
      "offset": 52
    },
    {
      "val": "2",
      "offset": 61
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 66
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 437876639,
  "status": "passed"
});
formatter.match({
  "location": "SearchRequestSteps.user_should_get_service_response()"
});
formatter.result({
  "duration": 102969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 44
    },
    {
      "val": "3",
      "offset": 50
    },
    {
      "val": " India",
      "offset": 55
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 65
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 94
    }
  ],
  "location": "SearchRequestSteps.verify_service_response_for_hotel(String,String,String,String,String)"
});
formatter.result({
  "duration": 30584526,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "This test case is to verify search request with multiple test data",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 27,
      "name": "@SearchHotels"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {After 2CHD (3\u002612), It\u0027s age of child}"
    },
    {
      "line": 30,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {after split \"~\", detail of guests of each room}"
    }
  ],
  "line": 31,
  "name": "Send search service request for details \"40\", \"15\", \"UnitedArabEmirates\", \"1\", \"2ADT-1CHD(3)\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User should get service response",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Verify service response for search service \"40\", \"15\", \" UnitedArabEmirates\", \"2ADT-1CHD(3)\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    4,
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "40",
      "offset": 41
    },
    {
      "val": "15",
      "offset": 47
    },
    {
      "val": "UnitedArabEmirates",
      "offset": 53
    },
    {
      "val": "1",
      "offset": 75
    },
    {
      "val": "2ADT-1CHD(3)",
      "offset": 80
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 96
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 3715807163,
  "status": "passed"
});
formatter.match({
  "location": "SearchRequestSteps.user_should_get_service_response()"
});
formatter.result({
  "duration": 26169,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "40",
      "offset": 44
    },
    {
      "val": "15",
      "offset": 50
    },
    {
      "val": " UnitedArabEmirates",
      "offset": 56
    },
    {
      "val": "2ADT-1CHD(3)",
      "offset": 79
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.verify_service_response_for_hotel(String,String,String,String,String)"
});
formatter.result({
  "duration": 24865501,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "This test case is to verify search request with multiple test data",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;this-test-case-is-to-verify-search-request-with-multiple-test-data;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 27,
      "name": "@SearchHotels"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 29,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {After 2CHD (3\u002612), It\u0027s age of child}"
    },
    {
      "line": 30,
      "value": "#Guest Details :1ADT-2CHD(3\u002612)~2ADT-0CHD {after split \"~\", detail of guests of each room}"
    }
  ],
  "line": 31,
  "name": "Send search service request for details \"365\", \"1\", \"Maldives\", \"1\", \"2ADT-1CHD(3)\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User should get service response",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "Verify service response for search service \"365\", \"1\", \" Maldives\", \"2ADT-1CHD(3)\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    4,
    5
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "365",
      "offset": 41
    },
    {
      "val": "1",
      "offset": 48
    },
    {
      "val": "Maldives",
      "offset": 53
    },
    {
      "val": "1",
      "offset": 65
    },
    {
      "val": "2ADT-1CHD(3)",
      "offset": 70
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 86
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1708570346,
  "status": "passed"
});
formatter.match({
  "location": "SearchRequestSteps.user_should_get_service_response()"
});
formatter.result({
  "duration": 53475,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "365",
      "offset": 44
    },
    {
      "val": "1",
      "offset": 51
    },
    {
      "val": " Maldives",
      "offset": 56
    },
    {
      "val": "2ADT-1CHD(3)",
      "offset": 69
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 85
    }
  ],
  "location": "SearchRequestSteps.verify_service_response_for_hotel(String,String,String,String,String)"
});
formatter.result({
  "duration": 37596064,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 43,
  "name": "Error message should be displayed if check-in date is past date",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-check-in-date-is-past-date",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 42,
      "name": "@PastCheckInDate"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "Send search service request for details \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \"\u003cDestination\u003e\", \"\u003cNo of Rooms\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "Verify Error message \"\u003cError Message\u003e\" should be displayed",
  "keyword": "Then "
});
formatter.examples({
  "line": 47,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-check-in-date-is-past-date;",
  "rows": [
    {
      "cells": [
        "Booking After",
        "No of Days Stays",
        "Destination",
        "No of Rooms",
        "Guests",
        "placeId",
        "Error Message"
      ],
      "line": 48,
      "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-check-in-date-is-past-date;;1"
    },
    {
      "cells": [
        "-1",
        "3",
        "India",
        "2",
        "1ADT-2CHD(3\u002612)~2ADT-0CHD",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
        "The dates.checkin must be a date after"
      ],
      "line": 49,
      "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-check-in-date-is-past-date;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 49,
  "name": "Error message should be displayed if check-in date is past date",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-check-in-date-is-past-date;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 42,
      "name": "@PastCheckInDate"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "Send search service request for details \"-1\", \"3\", \"India\", \"2\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "Verify Error message \"The dates.checkin must be a date after\" should be displayed",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "-1",
      "offset": 41
    },
    {
      "val": "3",
      "offset": 47
    },
    {
      "val": "India",
      "offset": 52
    },
    {
      "val": "2",
      "offset": 61
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 66
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 699247987,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "The dates.checkin must be a date after",
      "offset": 22
    }
  ],
  "location": "SearchRequestSteps.Error_message_should_be_displayed(String)"
});
formatter.result({
  "duration": 15351428,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 53,
  "name": "Error message should be displayed if checkout date is same or less than check-in date",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-checkout-date-is-same-or-less-than-check-in-date",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 52,
      "name": "@CheckOutDateLessThanOrSame_WithCheckInDate"
    }
  ]
});
formatter.step({
  "line": 55,
  "name": "Send search service request for details \"\u003cBooking After\u003e\", \"\u003cNo of Days Stays\u003e\", \"\u003cDestination\u003e\", \"\u003cNo of Rooms\u003e\", \"\u003cGuests\u003e\", \"\u003cplaceId\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "Verify Error message \"\u003cError Message\u003e\" should be displayed",
  "keyword": "Then "
});
formatter.examples({
  "line": 57,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-checkout-date-is-same-or-less-than-check-in-date;",
  "rows": [
    {
      "cells": [
        "Booking After",
        "No of Days Stays",
        "Destination",
        "No of Rooms",
        "Guests",
        "placeId",
        "Error Message"
      ],
      "line": 58,
      "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-checkout-date-is-same-or-less-than-check-in-date;;1"
    },
    {
      "cells": [
        "5",
        "-2",
        "India",
        "2",
        "1ADT-2CHD(3\u002612)~2ADT-0CHD",
        "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
        "The dates.checkout must be a date after dates.checkin"
      ],
      "line": 59,
      "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-checkout-date-is-same-or-less-than-check-in-date;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 59,
  "name": "Error message should be displayed if checkout date is same or less than check-in date",
  "description": "",
  "id": "this-feature-file-is-to-test-\"search\"-request;error-message-should-be-displayed-if-checkout-date-is-same-or-less-than-check-in-date;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 52,
      "name": "@CheckOutDateLessThanOrSame_WithCheckInDate"
    }
  ]
});
formatter.step({
  "line": 55,
  "name": "Send search service request for details \"5\", \"-2\", \"India\", \"2\", \"1ADT-2CHD(3\u002612)~2ADT-0CHD\", \"ChIJD7fiBh9u5kcRYJSMaMOCCwQ\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "Verify Error message \"The dates.checkout must be a date after dates.checkin\" should be displayed",
  "matchedColumns": [
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 41
    },
    {
      "val": "-2",
      "offset": 46
    },
    {
      "val": "India",
      "offset": 52
    },
    {
      "val": "2",
      "offset": 61
    },
    {
      "val": "1ADT-2CHD(3\u002612)~2ADT-0CHD",
      "offset": 66
    },
    {
      "val": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
      "offset": 95
    }
  ],
  "location": "SearchRequestSteps.send_search_service_request(String,String,String,String,String,String)"
});
formatter.result({
  "duration": 407657339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "The dates.checkout must be a date after dates.checkin",
      "offset": 22
    }
  ],
  "location": "SearchRequestSteps.Error_message_should_be_displayed(String)"
});
formatter.result({
  "duration": 16099514,
  "status": "passed"
});
});