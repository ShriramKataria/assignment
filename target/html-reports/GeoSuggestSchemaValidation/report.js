$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/resources/Features/GeoSuggest.feature");
formatter.feature({
  "line": 1,
  "name": "This feature file is to verify geo-suggest service",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Verify geo suggest service response",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@GeoSuggestSchemaValidation"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Send geo suggest service request for \"\u003cQuery\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Perform schema validation for geosuggest service response",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;",
  "rows": [
    {
      "cells": [
        "Query"
      ],
      "line": 10,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;;1"
    },
    {
      "cells": [
        "Paris"
      ],
      "line": 11,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;;2"
    },
    {
      "cells": [
        "India"
      ],
      "line": 12,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Verify geo suggest service response",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@GeoSuggestSchemaValidation"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Send geo suggest service request for \"Paris\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Perform schema validation for geosuggest service response",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Paris",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 2234342938,
  "status": "passed"
});
formatter.match({
  "location": "GeoSuggestSteps.perform_schema_validation_for_geo_suggest_service()"
});
formatter.result({
  "duration": 428496254,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Verify geo suggest service response",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@GeoSuggestSchemaValidation"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Send geo suggest service request for \"India\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Perform schema validation for geosuggest service response",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "India",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 633012424,
  "status": "passed"
});
formatter.match({
  "location": "GeoSuggestSteps.perform_schema_validation_for_geo_suggest_service()"
});
formatter.result({
  "duration": 21815696,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 16,
  "name": "Verify geo suggest service response for hotel object",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 15,
      "name": "@GeoSuggestResponseValidation"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "Send geo suggest service request for \"\u003cQuery\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "Verify service response for hotel",
  "keyword": "Then "
});
formatter.examples({
  "line": 20,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;",
  "rows": [
    {
      "cells": [
        "Query"
      ],
      "line": 21,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;1"
    },
    {
      "cells": [
        "Paris"
      ],
      "line": 22,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;2"
    },
    {
      "cells": [
        "India"
      ],
      "line": 23,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 22,
  "name": "Verify geo suggest service response for hotel object",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 15,
      "name": "@GeoSuggestResponseValidation"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "Send geo suggest service request for \"Paris\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "Verify service response for hotel",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Paris",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 698107369,
  "status": "passed"
});
formatter.match({
  "location": "GeoSuggestSteps.verify_service_response_for_hotel()"
});
formatter.result({
  "duration": 292925259,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Verify geo suggest service response for hotel object",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 15,
      "name": "@GeoSuggestResponseValidation"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "Send geo suggest service request for \"India\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "Verify service response for hotel",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "India",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 415866384,
  "status": "passed"
});
formatter.match({
  "location": "GeoSuggestSteps.verify_service_response_for_hotel()"
});
formatter.result({
  "duration": 14793918,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Verify cookies size and cookies Keys",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-cookies-size-and-cookies-keys",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 25,
      "name": "@GeoSuggestCookiesKeysValidation"
    }
  ]
});
formatter.step({
  "line": 27,
  "name": "Send geo suggest service request for \"India\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "Verify that number of cookies should be \"6\" when response came first time",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "Verify cookies Keys text received from server",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "Verify that Max age of cookies should be \"900\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "India",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 414402637,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 41
    }
  ],
  "location": "GeoSuggestSteps.Verify_that_number_of_cookies(String)"
});
formatter.result({
  "duration": 1091695,
  "status": "passed"
});
formatter.match({
  "location": "GeoSuggestSteps.Verify_cookies_Keys_text_received_from_server()"
});
formatter.result({
  "duration": 212195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "900",
      "offset": 42
    }
  ],
  "location": "GeoSuggestSteps.Verify_expiry_of_cookies(String)"
});
formatter.result({
  "duration": 913065,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 35,
  "name": "Verify geo suggest service response for hotel object",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 34,
      "name": "@GeoSuggestResponseValidation"
    }
  ]
});
formatter.step({
  "line": 36,
  "name": "Send geo suggest service request for \"\u003cQuery\u003e\"",
  "keyword": "When "
});
formatter.examples({
  "line": 39,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;",
  "rows": [
    {
      "cells": [
        "NoOfRooms",
        "Room1"
      ],
      "line": 40,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;1"
    },
    {
      "cells": [
        "2",
        "1ADT-2CHD~2ADT-3CHD"
      ],
      "line": 41,
      "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 41,
  "name": "Verify geo suggest service response for hotel object",
  "description": "",
  "id": "this-feature-file-is-to-verify-geo-suggest-service;verify-geo-suggest-service-response-for-hotel-object;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 34,
      "name": "@GeoSuggestResponseValidation"
    }
  ]
});
formatter.step({
  "line": 36,
  "name": "Send geo suggest service request for \"\u003cQuery\u003e\"",
  "keyword": "When "
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cQuery\u003e",
      "offset": 38
    }
  ],
  "location": "GeoSuggestSteps.send_geo_suggest_service_request_for(String)"
});
formatter.result({
  "duration": 427666248,
  "status": "passed"
});
});