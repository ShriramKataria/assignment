## Assignment Title:
#### Automation framework to automate REST service using REST Assured Tool. This cover the automation of below 2 services:
* Geo-Suggest -GET
* Search Request-POST

## Framework:
#### In order to automate above service I used Behaviour Driven Development framework.

##Tools and Technologies
* Programming Langauge: Java
* Build Tool: Maven
* Unit Testing Framework: JUnit
* REST Assured

## Requirements:
#### In order to run this project you need to have following installed locally:

* Maven 3 or above version (e.g. 3.6.1)
* Java 1.8 or above
* Junit 4 (E.g.4.12)
## High Level Scenarios Covered:
#### Geo-Suggest -GET
* Search by Country name
* Search by Hotel name
* Search by City name
* Response schema validation
* Cookies Max Age
* Cookies Key Text
* Verify number of cookies received from server first time
* Verify number of cookies received from server 2nd time when request is sent with cookies (Sometimes server respond 2 cookies sometimes not, Intermittent issue)
* Response validation for Hotels object
* Response validations for Location object
* Scenario when no result is returned
#### Search Request-Post
* Response schema validation
* Handled dynamic dates in Request
* Handled dynamic room and guests
* Check-in date is past date
* Check-out date is before check-in date
* Room request after 1 year,3months,6months etc..
* Cookies Max Age
* Cookies Key Text
* Verify number of cookies received from server first time
* Verify response
* Covered No of Rooms,Guests,check-in checkout date combinations

## Run Test:

* Clone the project in your local
* Go to Project directry
* Execute command to run all Test
`mvn clean test`
* In order to clean project run 
`mvn clean`
* Command tor run single Test
`mvn -Dtest=GeoSuggestRunnerTest test` where "GeoSuggestRunnerTest" is test name
* Or Run from IDE like Eclipse, intellij by clicking on Run Button

## Reporting:
#### Below is 2 types of report you can refer after a successfull run
* Cucumber report: 
* html report: 
#### Refer Cucumber report into `/target/cucumber-report` directories after a successful run.If using CI, these individual reports can be joined using plugins such as the Jenkins Cucumber-JVM-Reports plugin
#### Refer html report into `/target/html-report` directories after a successful run







## Author
* Shriram Kataria